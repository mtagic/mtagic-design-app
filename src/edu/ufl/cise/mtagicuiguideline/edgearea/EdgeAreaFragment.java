package edu.ufl.cise.mtagicuiguideline.edgearea;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import edu.ufl.cise.mtagicuiguideline.R;

public class EdgeAreaFragment extends Fragment {
	
	public EdgeAreaFragment(){}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_edge_area, container, false);
        
        WrapContentHeightViewPager pager = (WrapContentHeightViewPager) rootView.findViewById(R.id.viewpager);
        pager.setAdapter(new ViewPagerAdapterEdgeArea(getFragmentManager()));
        
        WebView edgeAreaWebView = (WebView) rootView.findViewById(R.id.edgeAreaWebView);
  		edgeAreaWebView.getSettings().setBuiltInZoomControls(false);
  		edgeAreaWebView.setVerticalScrollBarEnabled(true);
  		edgeAreaWebView.setHorizontalScrollBarEnabled(true);
  		edgeAreaWebView.loadUrl("file:///android_asset/edge-area.html");
  		
        return rootView;
    }
}
