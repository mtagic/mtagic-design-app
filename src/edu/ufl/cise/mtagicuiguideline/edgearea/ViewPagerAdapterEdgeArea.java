package edu.ufl.cise.mtagicuiguideline.edgearea;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class ViewPagerAdapterEdgeArea extends FragmentStatePagerAdapter {

	private String example1 = "Corner Case";
	private String example2 = "Edge Case";
	
	public ViewPagerAdapterEdgeArea(FragmentManager mgr) {
		super(mgr);
	}

	@Override
	public int getCount() {
		return(2);
	}

	@Override
	public Fragment getItem(int position) {
		if(position == 0) 
			return new EdgeAreaExample1Fragment();
		else 
			return new EdgeAreaExample2Fragment();
	}

	@Override
	public String getPageTitle(int position) {
		return position == 0 ? example1:example2;
	}
}
