package edu.ufl.cise.mtagicuiguideline.edgearea;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;
import edu.ufl.cise.mtagicuiguideline.R;

public class EdgeAreaExample1Fragment extends Fragment implements OnClickListener {

	private int touchCounter1,touchCounter2;
	private ImageButton mSquareButton1,mSquareButton2;
	private TextView mTouchCounterTextView1,mTouchCounterTextView2;
	
	public EdgeAreaExample1Fragment(){
		touchCounter1 = 0;
		touchCounter2 = 0;
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_edge_area_example1, container, false);
		
		mSquareButton1 = (ImageButton)rootView.findViewById(R.id.imageAtEdge1Example1);
		mSquareButton1.setOnClickListener(this);
		
		mTouchCounterTextView1 = (TextView)rootView.findViewById(R.id.imageAtEdge1Example1Cnt);
		mTouchCounterTextView1.setText(Integer.toString(touchCounter1));
		
		mSquareButton2 = (ImageButton)rootView.findViewById(R.id.imageAtEdge2Example1);
		mSquareButton2.setOnClickListener(this);
		
		mTouchCounterTextView2 = (TextView)rootView.findViewById(R.id.imageAtEdge2Example1Cnt);
		mTouchCounterTextView2.setText(Integer.toString(touchCounter2));
		
        return rootView;
    }
	
	@Override
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.imageAtEdge1Example1:
			touchCounter1++;
			if(touchCounter1 >= 20)
				touchCounter1 = 0;
			mTouchCounterTextView1.setText(Integer.toString(touchCounter1));
			break;
		case R.id.imageAtEdge2Example1:
			touchCounter2++;
			if(touchCounter2 >= 20)
				touchCounter2 = 0;
			mTouchCounterTextView2.setText(Integer.toString(touchCounter2));
			break;
		default:
			Log.e("MTAGIC","wrong OnClick handler called");
			break;
		}
	}
}
