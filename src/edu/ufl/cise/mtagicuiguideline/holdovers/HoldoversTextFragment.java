package edu.ufl.cise.mtagicuiguideline.holdovers;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import edu.ufl.cise.mtagicuiguideline.R;

public class HoldoversTextFragment extends Fragment {
	public HoldoversTextFragment(){}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_holdovers_text, container, false);
		
		WebView holdoversWebView = (WebView) rootView.findViewById(R.id.holdoverAreaWebView);
  		holdoversWebView.getSettings().setBuiltInZoomControls(false);
  		holdoversWebView.setVerticalScrollBarEnabled(true);
  		holdoversWebView.setHorizontalScrollBarEnabled(true);
  		holdoversWebView.loadUrl("file:///android_asset/holdovers.html");
  		
		return rootView;

	}
}
