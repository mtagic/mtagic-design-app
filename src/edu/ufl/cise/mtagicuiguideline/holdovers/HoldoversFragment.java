package edu.ufl.cise.mtagicuiguideline.holdovers;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import edu.ufl.cise.mtagicuiguideline.R;

public class HoldoversFragment extends Fragment {
	
	public HoldoversFragment(){}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_holdovers, container, false);
        ViewPager pager = (ViewPager) rootView.findViewById(R.id.viewpager_holdovers);
        pager.setAdapter(new ViewPagerAdapterHoldovers(getFragmentManager()));
        
        return rootView;
    }
}
