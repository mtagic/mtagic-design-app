package edu.ufl.cise.mtagicuiguideline.holdovers;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import edu.ufl.cise.mtagicuiguideline.R;

public class HoldoversDemoFragment extends Fragment implements OnClickListener {

	//Image Button instance
	private ImageButton mSquareButton;
	private TextView mTouchCounterTextView;
	private TextView mDemoInfoTextView;
	private EditText mSetTimeEditText;
	private Button mSetTimeButton;
	
	//seconds time
	private long timeInMilliSeconds;
	//touch counter
    private int touchCounter;
    //timer set boolean flag
    private boolean bTimerNonZero;
    
	public HoldoversDemoFragment(){
		touchCounter = 0;
		bTimerNonZero = true;
		timeInMilliSeconds = 0;
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_holdovers_demo, container, false);
				
		mSquareButton = (ImageButton)rootView.findViewById(R.id.demo_image_button);
		mSquareButton.setOnClickListener(this);
		
		mTouchCounterTextView = (TextView)rootView.findViewById(R.id.demo_textview);
		mTouchCounterTextView.setText(Integer.toString(touchCounter));
		
		mDemoInfoTextView = (TextView)rootView.findViewById(R.id.demo_info_textview_2);
		mDemoInfoTextView.setVisibility(View.INVISIBLE);
		
		mSetTimeEditText = (EditText)rootView.findViewById(R.id.edittext_input_time);
		
		mSetTimeButton = (Button)rootView.findViewById(R.id.button_set_time);
		mSetTimeButton.setOnClickListener(this);
		
		return rootView;
    }

	@Override
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.demo_image_button:
			touchCounter++;
			if (touchCounter == 7) {
				Toast.makeText(getActivity(), "You might want to set the timer first!", Toast.LENGTH_LONG).show();
			}
			if(touchCounter >= 99)
				touchCounter = 0;
			mTouchCounterTextView.setText(Integer.toString(touchCounter));
			if(bTimerNonZero && timeInMilliSeconds > 0) {	
				final String inputTime = mSetTimeEditText.getText().toString();
				
				mSquareButton.setBackgroundResource(R.drawable.ic_gray_square_icon);
				mSetTimeButton.setClickable(false);
				mSquareButton.setClickable(false);
				mSetTimeEditText.setTextColor(Color.RED);
				mSetTimeEditText.setFocusable(false);
				mSetTimeEditText.setFocusableInTouchMode(false);
				mDemoInfoTextView.setVisibility(View.VISIBLE);
				
				new CountDownTimer(timeInMilliSeconds, 1000) {
					public void onTick(long millisUntilFinished) {
						mSetTimeEditText.setText(String.valueOf(millisUntilFinished / 1000));
					}			
					public void onFinish() {
						touchCounter = 0;
						mTouchCounterTextView.setText(Integer.toString(touchCounter));
						mDemoInfoTextView.setVisibility(View.INVISIBLE);
						mSetTimeButton.setClickable(true);
						mSetTimeButton.setText(R.string.holdover_demo_button_text);
						mSquareButton.setClickable(true);
						mSetTimeEditText.setFocusable(true);
						mSetTimeEditText.setFocusableInTouchMode(true);
						mSetTimeEditText.requestFocus();
						mSetTimeEditText.setTextColor(Color.BLACK);
						mSetTimeEditText.setText(inputTime);
						mSquareButton.setBackgroundResource(R.drawable.ic_square_icon);
						timeInMilliSeconds = 0;
					}
				}.start();
			}
			else if (!bTimerNonZero) {
				if(mSetTimeButton.getText() == getActivity().getString(R.string.holdover_demo_button_text_set)) {
					mSetTimeButton.setText(R.string.holdover_demo_button_text);
					mDemoInfoTextView.setVisibility(View.VISIBLE);
					touchCounter = -1;
				}
				else 
					mDemoInfoTextView.setVisibility(View.INVISIBLE);
			}
			break;
		case R.id.button_set_time:
			final String inputTime = mSetTimeEditText.getText().toString();
			try {
				timeInMilliSeconds = Long.parseLong(inputTime);
				timeInMilliSeconds *= 1000;
				if(timeInMilliSeconds > 0) {
					bTimerNonZero = true;
					mDemoInfoTextView.setVisibility(View.INVISIBLE);
					mDemoInfoTextView.setText(R.string.holdover_demo_info_2);
				}
				else {
					bTimerNonZero = false;
					mDemoInfoTextView.setText(R.string.holdover_demo_info_0_threshold);
				}
				mSetTimeEditText.clearFocus();
				mSetTimeButton.requestFocus();
				
				InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(mSetTimeEditText.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
				
				touchCounter = 0;
				mTouchCounterTextView.setText(Integer.toString(touchCounter));
				mSetTimeButton.setText(R.string.holdover_demo_button_text_set);
			}
			catch(NumberFormatException nfe) {
				nfe.printStackTrace();
				Toast.makeText(getActivity(), "Wrong input!", Toast.LENGTH_LONG).show();
			}
			break;
		default:
			Log.e("MTAGIC","wrong OnClick handler called");
			break;
		}
	}
}