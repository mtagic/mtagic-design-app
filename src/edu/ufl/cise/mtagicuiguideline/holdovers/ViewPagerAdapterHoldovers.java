package edu.ufl.cise.mtagicuiguideline.holdovers;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

public class ViewPagerAdapterHoldovers extends FragmentStatePagerAdapter {

	private String example1 = "Holdovers";
	private String example2 = "Demo";
	
	public ViewPagerAdapterHoldovers(FragmentManager mgr) {
		super(mgr);
	}

	@Override
	public int getCount() {
		return(2);
	}

	@Override
	public Fragment getItem(int position) {
		if(position == 0) 
			return new HoldoversTextFragment();
		else 
			return new HoldoversDemoFragment();
	}

	@Override
	public String getPageTitle(int position) {
		return position == 0 ? example1:example2;
	}
}
