package edu.ufl.cise.mtagicuiguideline;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.TextView;

public class ActiveAreaFragment extends Fragment implements OnClickListener {
	
	private int touchCounter;
	private ImageButton mSquareButton;
	private TextView mTouchCounterTextView;
	
	public ActiveAreaFragment(){
		touchCounter = 0;
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_active_area, container, false);
        
        WebView activeAreaWebView = (WebView) rootView.findViewById(R.id.activeAreaWebView);
		activeAreaWebView.getSettings().setBuiltInZoomControls(false);
		activeAreaWebView.setVerticalScrollBarEnabled(true);
		activeAreaWebView.setHorizontalScrollBarEnabled(true);
		activeAreaWebView.loadUrl("file:///android_asset/active-area.html");
		
		mSquareButton = (ImageButton)rootView.findViewById(R.id.imagePadded);
		mSquareButton.setOnClickListener(this);
		
		mTouchCounterTextView = (TextView)rootView.findViewById(R.id.imagePaddedCounter);
		mTouchCounterTextView.setText(Integer.toString(touchCounter));
		
        return rootView;
    }
	
	@Override
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.imagePadded:
			touchCounter++;
			if(touchCounter >= 20)
				touchCounter = 0;
			mTouchCounterTextView.setText(Integer.toString(touchCounter));
			break;
		default:
			Log.e("MTAGIC","wrong OnClick handler called");
			break;
		}
	}
}
