package edu.ufl.cise.mtagicuiguideline;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.TextView;

public class AndroidUIFragment extends Fragment implements OnClickListener {
	
	private int touchCounter1,touchCounter2;
	private ImageButton mSquareButton1,mSquareButton2;
	private TextView mTouchCounterTextView1,mTouchCounterTextView2;
	
	public AndroidUIFragment(){
		touchCounter1 = 0;
		touchCounter2 = 0;
	}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.fragment_android_ui, container, false);
        
        WebView androidUIWebView = (WebView) rootView.findViewById(R.id.androidUIWebView);
		androidUIWebView.getSettings().setBuiltInZoomControls(false);
		androidUIWebView.setVerticalScrollBarEnabled(true);
		androidUIWebView.setHorizontalScrollBarEnabled(true);
		androidUIWebView.loadUrl("file:///android_asset/android-ui-page.html");
		
		mSquareButton1 = (ImageButton)rootView.findViewById(R.id.imageSmall);
		mSquareButton1.setOnClickListener(this);
		
		mTouchCounterTextView1 = (TextView)rootView.findViewById(R.id.imageSmallCnt);
		mTouchCounterTextView1.setText(Integer.toString(touchCounter1));
		
		mSquareButton2 = (ImageButton)rootView.findViewById(R.id.imageBig);
		mSquareButton2.setOnClickListener(this);
		
		mTouchCounterTextView2 = (TextView)rootView.findViewById(R.id.imageBigCnt);
		mTouchCounterTextView2.setText(Integer.toString(touchCounter2));
		
        return rootView;
    }
	
	@Override
	public void onClick(View v) {
		switch(v.getId()) {
		case R.id.imageSmall:
			touchCounter1++;
			if(touchCounter1 >= 20)
				touchCounter1 = 0;
			mTouchCounterTextView1.setText(Integer.toString(touchCounter1));
			break;
		case R.id.imageBig:
			touchCounter2++;
			if(touchCounter2 >= 20)
				touchCounter2 = 0;
			mTouchCounterTextView2.setText(Integer.toString(touchCounter2));
			break;
		default:
			Log.e("MTAGIC","wrong OnClick handler called");
			break;
		}
	}
}
