package edu.ufl.cise.mtagicuiguideline;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

public class HomeFragment extends Fragment {

	public HomeFragment(){}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_home, container, false);
		
		WebView homeWebView = (WebView) rootView.findViewById(R.id.homeWebView);
		homeWebView.getSettings().setBuiltInZoomControls(false);
		homeWebView.setVerticalScrollBarEnabled(true);
		homeWebView.setHorizontalScrollBarEnabled(true);
		homeWebView.loadUrl("file:///android_asset/home-page.html");
		
		return rootView;
	}
}
